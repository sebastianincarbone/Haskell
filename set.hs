
module Set(losQuePertenecen, sinRepetidos, unirTodos) where

data Conjunto a = S [a] 

emptyS :: Set a
emptyS = S []

addS :: Eq a => a -> Set a -> Set a
addS x (S as)  = 
    if belongs x (S as) 
    then    (S as)
    else    (S (x:as))

belongs :: Eq a => a -> Set a -> Bool
belongs x (S []) = False
belongs x (S (a:as) ) =
    if x == a 
    then    True
    else    belongs x (S as)

--sizeS :: Eq a => Set a -> Int
--removeS :: Eq a => a -> Set a -> Set a
--unionS :: Eq a => Set a -> Set a -> Set a
--intersectionS :: Eq a => Set a -> Set a -> Set a
--setToList :: Eq a => Set a -> [a]
