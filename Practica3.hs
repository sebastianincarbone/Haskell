data Tree a = EmptyT |NodeT a (Tree a) (Tree a)

data Persona = Persona {name:: String, age:: Int}

---- 1----------------------------
persona1 :: Persona
persona1 = Persona "seba" 22

persona2 :: Persona
persona2 = Persona "gaston" 20

varTree :: Tree Int 
--varTree = NodeT persona1 (NodeT persona2 EmptyT EmptyT) EmptyT
varTree = NodeT 1 (NodeT 2 (NodeT 4 EmptyT EmptyT) EmptyT) (NodeT 3 (NodeT 5 EmptyT EmptyT) (NodeT 6 (NodeT 7 EmptyT EmptyT) (NodeT 8 EmptyT EmptyT)))
sumarT :: Tree Int -> Int
sumarT EmptyT = 0
sumarT (NodeT n br bl) = n + (sumarT br) + (sumarT bl) 

sizeT :: Tree a -> Int
sizeT EmptyT = 0
sizeT (NodeT n br bl) = 1 + (sizeT br) + (sizeT bl) 

mapDobleT :: Tree Int -> Tree Int
mapDobleT EmptyT =  EmptyT
mapDobleT (NodeT n br bl) =  NodeT (n*2) (mapDobleT br) (mapDobleT bl) 

len :: [a] -> Int
len [] = 0
len (x:xs) = 1 + len xs

mapLongitudT :: Tree String -> Tree Int
mapLongitudT EmptyT = EmptyT 
mapLongitudT (NodeT s br bl) = NodeT (len s) (mapLongitudT br) (mapLongitudT bl)

perteneceT :: Eq a => a -> Tree a -> Bool
perteneceT a EmptyT = False
perteneceT a (NodeT b br bl) =  if a == b then True else (perteneceT a br) || (perteneceT a bl)

nIgualA :: Eq a => a -> a -> Int
nIgualA a b = if a == b then 1 else 0

aparicionesT :: Eq a => a -> Tree a -> Int
aparicionesT a EmptyT =  0
aparicionesT a (NodeT n br bl) = (nIgualA a n) + (aparicionesT a br) + (aparicionesT a bl) 


-- promedioEdad Traido de la practica 2 donde se define persona!
promedioEdad :: [Persona] -> Int
promedioEdad [] = 0   
promedioEdad ((Persona n1 e1):ps) = 
        div (e1+promedioEdad ps) ((len ps)+1)

append ::[a]-> [a]-> [a]
append [] ys = ys
append (x:xs) ys = x:append xs ys

deArbolALista :: Tree Persona -> [Persona]
deArbolALista EmptyT = []
deArbolALista (NodeT p br bl) = p: append (deArbolALista br) (deArbolALista bl)

promedioEdadesT :: Tree Persona -> Int
promedioEdadesT EmptyT = 0
promedioEdadesT (NodeT p br bl) = promedioEdad (deArbolALista (NodeT p br bl))

contarLeaves :: Tree a -> Int
contarLeaves EmptyT = 1 
contarLeaves (NodeT n br bl) = (contarLeaves br) + (contarLeaves bl)

leaves :: Tree a -> [a]
leaves EmptyT = []
leaves (NodeT a EmptyT EmptyT) = [a]
leaves (NodeT a br bl) = append (leaves br) (leaves bl)

heightT :: Tree a -> Int
heightT EmptyT = 1
heightT (NodeT a br bl) =  if heightT br > heightT bl then (heightT br) + 1 else (heightT bl) + 1

contarNoHojas :: Tree a -> Int
contarNoHojas EmptyT = 0
contarNoHojas (NodeT a br bl) = 1 + (contarNoHojas br) + (contarNoHojas bl)

espejoT :: Tree a -> Tree a
espejoT EmptyT = EmptyT
espejoT (NodeT a br bl) = (NodeT a (espejoT bl) (espejoT br))

listInOrder :: Tree Persona -> [Persona]
listInOrder EmptyT = []
listInOrder (NodeT a EmptyT EmptyT) = [a]
listInOrder (NodeT a br bl) = append (listInOrder br) (append [a] (listInOrder bl))

listPreOrder :: Tree Persona -> [Persona]
listPreOrder EmptyT = []
listPreOrder (NodeT a br bl) = [a] ++ (deArbolALista br) ++ (deArbolALista bl)

listPosOrder :: Tree Persona -> [Persona]
listPosOrder EmptyT = []
listPosOrder (NodeT a br bl) = append (listInOrder br) (append (listInOrder bl) [a])
 
concatenarListasT :: Tree [a] -> [a]
concatenarListasT EmptyT = []
concatenarListasT (NodeT xs br bl) =  xs ++ (concatenarListasT br) ++ (concatenarListasT bl)

levelN :: Int -> Tree a -> [a]
levelN n EmptyT = []
levelN n (NodeT a br bl) = 
        if n == 0
        then [a] ++ (levelN (n-1) br) ++ (levelN (n-1) bl)
        else (levelN (n-1) br) ++ (levelN (n - 1) bl)


listPerLevel :: Tree a -> [[a]]
listPerLevel EmptyT = []
listPerLevel (NodeT a br bl) = [a]: juntarElementos (listPerLevel br) (listPerLevel bl)
--listPerLevel t = listarNiveles (heightT t) t

--aux 
--listarNiveles :: int ->  t   -> [[a]]
--listarNiveles 0 t = (levelN 0 t) : []
--listarNiveles n t = (levelN 0 t) : listaNiveles (n-1) t

juntarElementos :: [[a]] -> [[a]] -> [[a]]
juntarElementos [] [] = [[]]
juntarElementos [] (y:ys) = [y]
juntarElementos (y:ys) [] = [y]
juntarElementos (x:xs) (y:ys) = (x ++ y): juntarElementos xs ys

widthT :: Tree a -> Int
widthT t = listaMasLarga (listPerLevel t)

-- Aux
listaMasLarga :: [[a]] -> Int
listaMasLarga [] = 0
listaMasLarga (xs:xss) = if (len xs) > (listaMasLarga xss)
			then len xs
			else listaMasLarga xss

ramaDerecha :: Tree a -> [a] 
ramaDerecha EmptyT = []
ramaDerecha (NodeT a bl br) = a: (ramaDerecha br)

ramaMasLarga :: Tree a -> [a]
ramaMasLarga EmptyT = []
ramaMasLarga (NodeT a br bl) =  if heightT br > heightT bl then a: ramaMasLarga br else a: ramaMasLarga bl

--todosLosCaminos2 :: Tree a -> [[a]]
--todosLosCaminos2 EmptyT = []
--todosLosCaminos2 (NodeT a EmptyT EmptyT) = [a:[]]
--todosLosCaminos2 (NodeT a bl br) = agregarATodos a (todosLosCaminos2 bl ++ todosLosCaminos2 br) 

todosLosCaminos :: Tree a -> [[a]]
todosLosCaminos EmptyT = []
todosLosCaminos (NodeT p br bl) = 
        if esHoja (NodeT p br bl)
        then agregarHojas p (todosLosCaminos br ++ todosLosCaminos bl)
        else agregarATodos p (todosLosCaminos br ++ todosLosCaminos bl)

agregarATodos :: a ->[[a]] -> [[a]]
agregarATodos x [] = []
agregarATodos x (ys:yss) = (x:ys) : agregarATodos x yss 

agregarHojas :: a ->[[a]] -> [[a]]
agregarHojas x [] = [x:[]]
agregarHojas x (ys:yss) = (x:ys) : agregarHojas x yss 

esHoja :: Tree a -> Bool
esHoja (NodeT a EmptyT EmptyT) = True
esHoja _ = False
---- 2----------------------------
data Dir = Izq | Der
data Objeto = Tesoro | Chatarra
data Mapa = Cofre Objeto |Bifurcacion Objeto Mapa Mapa

mapT :: Mapa
mapT = Bifurcacion Chatarra (Bifurcacion Chatarra (Cofre Chatarra) (Bifurcacion Chatarra (Cofre Chatarra) (Cofre Tesoro))) (Cofre Tesoro)
mapsT :: Mapa
mapsT = Bifurcacion Chatarra (Bifurcacion Chatarra (Cofre Chatarra) (Cofre Chatarra)) (Cofre Chatarra)

hayTesoro :: Mapa -> Bool
hayTesoro (Cofre t) = esTesoro t
hayTesoro (Bifurcacion t bl br) =  esTesoro t || hayTesoro bl || hayTesoro br

--aux
esTesoro :: Objeto -> Bool
esTesoro Tesoro = True
esTesoro _ = False

hayTesoroEn :: [Dir] -> Mapa -> Bool
hayTesoroEn [] (Cofre t) = esTesoro t
hayTesoroEn ds (Cofre t) = False
hayTesoroEn [] (Bifurcacion t bl br) =  esTesoro t 
hayTesoroEn (d:ds) (Bifurcacion t bl br) =  
        if esIzquierda d 
        then esTesoro t || hayTesoroEn ds bl 
        else esTesoro t || hayTesoroEn ds br 

--aux
esIzquierda :: Dir -> Bool
esIzquierda Izq = True
esIzquierda Der = False

caminoAlTesoro :: Mapa -> [Dir]
caminoAlTesoro (Cofre a) = []
caminoAlTesoro (Bifurcacion a bl br) = 
        if (hayTesoroEn (caminoAlTesoro br) br) 
        then Der:caminoAlTesoro br
        else Izq:caminoAlTesoro bl

caminoRamaMasLarga :: Mapa -> [Dir]
caminoRamaMasLarga (Cofre a) = []
caminoRamaMasLarga (Bifurcacion a bl br) = 
        if len(caminoRamaMasLarga bl) > len(caminoRamaMasLarga br) 
        then Izq:(caminoRamaMasLarga bl) 
        else Der:(caminoRamaMasLarga br)

tesorosPerLevel :: Mapa -> [[Objeto]]
tesorosPerLevel (Cofre a) = if esTesoro a then [a:[]] else []
tesorosPerLevel (Bifurcacion a bl br) = 
        if esTesoro a 
        then [a]: unirListasPerLevel(tesorosPerLevel bl)(tesorosPerLevel br)
        else []: unirListasPerLevel(tesorosPerLevel bl)(tesorosPerLevel br)

--aux
unirListasPerLevel :: [[a]] -> [[a]] -> [[a]]
unirListasPerLevel [] [] = []
unirListasPerLevel xs [] = xs
unirListasPerLevel [] ys = ys
unirListasPerLevel (x:xs) (y:ys) = (x++y) : unirListasPerLevel xs ys 

todosLosCaminosM :: Mapa -> [[Dir]]
todosLosCaminosM (Cofre a) = []
todosLosCaminosM (Bifurcacion p bl br) = (agregarHojasM  Izq (todosLosCaminosM bl)) ++ (agregarHojasM  Der (todosLosCaminosM br))

agregarHojasM :: Dir ->[[Dir]] -> [[Dir]]
agregarHojasM x [] = [x:[]]
agregarHojasM x (ys:yss) = (x:ys) : agregarHojasM x yss 

---- 3----------------------------
data Exp = Constante Int |ConsExpUnaria OpUnaria Exp |ConsExpBinaria OpBinaria Exp Exp
data OpUnaria = Neg
data OpBinaria = Suma | Resta | Mult | Div

cuenta :: Exp
cuenta = ConsExpBinaria Suma (ConsExpBinaria Resta (Constante 4) (Constante 0)) (Constante 2)

eval :: Exp -> Int
eval (Constante a) = a
eval (ConsExpUnaria Neg a) = -1 * (eval a)
eval (ConsExpBinaria o x y) = resolver o (eval x) (eval y)

resolver :: OpBinaria -> Int -> Int -> Int
resolver Suma x y = x + y
resolver Resta x y = x - y
resolver Mult x y = x * y
--resolver Div x y = x / y

simplificar :: Exp -> Exp
simplificar (Constante a) = (Constante a)
simplificar (ConsExpUnaria Neg a) = (ConsExpUnaria Neg a)
simplificar (ConsExpBinaria o x y) = resolverPorCero (ConsExpBinaria o (simplificar x) (simplificar y))

resolverPorCero :: Exp -> Exp
resolverPorCero (ConsExpBinaria Suma (Constante 0) y)  = y
resolverPorCero (ConsExpBinaria Suma y (Constante 0))  = y
resolverPorCero (ConsExpBinaria Resta  (Constante 0) y) = y
resolverPorCero (ConsExpBinaria Resta  x (Constante 0)) = (ConsExpUnaria Neg x)
resolverPorCero (ConsExpBinaria Mult x (Constante 0))  = (Constante 0)
resolverPorCero (ConsExpBinaria Mult (Constante 0) y)  = (Constante 0)
--resolver Div x y = x / y

--------------------------------Clase Martes 10-4-18
--type Person = String
--
--data Charla = Silencio Int | Habla Person Int | AlMismoTiempo Charla Charla | PorTurnos Charla Charla
--
--data Evento = MkEvento Person Int Int
--
--duracion :: Charla -> Int
--duracion (Silencio d) = d
--duracion (Habla p d) = d
--duracion (AlMismoTiempo c1 c2) = max (duracion c1) (duracion c2)
--duracion (PorTurnos c1 c2) = duracion c1 + duracion c2
--
--esSilencio :: Charla -> Bool
--esSilencio (Silencio t) = True
--esSilencio (Habla p t) = False
--esSilencio (AlMismoTiempo c1 c2) = esSilencio c1 && esSilencio c2
--esSilencio (PorTurnos c1 c2) = esSilencio c1 && esSilencio c2
--
--transcripcion :: Charla -> [Evento]
--transcripcion (Silencio t) = [] 
--transcripcion (Habla p t) = [MkEvento p 0 t]
--transcripcion (AlMismoTiempo c1 c2) =  transcripcion c1 ++ transcripcion c2
--transcripcion (PorTurnos c1 c2) = transcripcion c1 ++ nuevoTiempo (duracion c1) (transcripcion c2)
--
----Aux
--
--nuevoTiempo :: Int -> Charla -> [Evento]
--nuevoTiempo t [] = [] 
--nuevoTiempo t (e:es) = (demorar e t):( nuevoTiempo t es)
--
--demorar :: Evento -> Int -> Evento
--demorar (MkEvento p t1 t2) t0 = MkEvento p t0 t2
----------
