-- definiciones----------------------------

data Dir = Este |Oeste |Sur |Norte

data Persona = Persona {name:: String, age:: Int}

data TipoDePokemon = Agua |Planta |Fuego
        deriving (Eq)
        -- :set -u
data Pokemon = Pokemon TipoDePokemon Int 
        deriving (Eq)

data Entrenador = Entrenador String [Pokemon]
        deriving (Eq)

data Pizza = Prepizza |Agregar Ingrediente Pizza
        deriving (Show, Eq)

data Ingrediente = Salsa |Queso |Jamon |AceitunasVerdes Int
        deriving (Show, Eq)

data Objeto = Cacharro | Tesoro
        deriving (Show, Eq)

data Camino = Fin | Cofre [Objeto] Camino | Nada Camino
        deriving (Show, Eq)

data ListaNoVacia a = Unit a | Cons a (ListaNoVacia a)
        deriving (Show, Eq)

data T a = 
        A 
       |B a 
       |C a a 
       |D (T a) 
       |E a (T a)

       deriving (Show, Eq)       
-- 1 --------------------------------------

opuesto :: Dir -> Dir
opuesto Este = Oeste
opuesto Norte = Sur
opuesto Sur = Norte
opuesto Oeste = Este

siguiente :: Dir -> Dir
siguiente Este = Sur
siguiente Oeste = Norte
siguiente Norte = Este
siguiente Sur = Oeste

-- 2 --------------------------------------

nombre :: Persona -> String
nombre (Persona nombre edad) = nombre

listNombre :: [Persona] -> [String]
listNombre [] = []
listNombre ((Persona nombre edad ):ps)= nombre:listNombre ps 

edad :: Persona -> Int
edad (Persona nombre edad) = edad

crecer :: Persona -> Persona
crecer (Persona nombre edad) = Persona nombre (edad + 1)

cambioDeNombre :: String -> Persona -> Persona
cambioDeNombre n (Persona nn e) = Persona n e

esMenorQueLaOtra :: Persona -> Persona -> Bool
esMenorQueLaOtra (Persona n1 e1) (Persona n2 e2) = if e1 < e2  then True else False

mayoresA :: Int -> [Persona] -> [Persona]
mayoresA e [] = [] 
mayoresA e ((Persona n1 e1):ps) = 
        if e > e1
        then (Persona n1 e1):mayoresA e (ps)
        else mayoresA e (ps)

promedioEdad :: [Persona] -> Int
promedioEdad [] = 0   
promedioEdad ((Persona n1 e1):ps) = 
        div (e1+promedioEdad ps) ((len ps)+1)
        
elMasViejo :: [Persona] -> Persona
elMasViejo (p:[]) = p
elMasViejo (p:ps) = 
        if edad p  > edad (elMasViejo ps) 
        then p 
        else elMasViejo ps

-- 3 --------------------------------------

leGana :: TipoDePokemon -> TipoDePokemon -> Bool
leGana Agua t2 =
        if t2 == Fuego
        then True
        else False
leGana Fuego t2 =
        if t2 == Planta
        then True
        else False
leGana Planta t2 =
        if t2 == Agua
        then True
        else False

leGanaP :: Pokemon -> Pokemon -> Bool
leGanaP p1 p2 = leGana (tipoP p1) (tipoP p2) 

capturarPokemon :: Pokemon -> Entrenador -> Entrenador
capturarPokemon p (Entrenador  n ps) = Entrenador n (p:ps) 

cantidadDePokemons :: Entrenador -> Int
cantidadDePokemons (Entrenador n ps) = len ps

cantidadDePokemonsDeTipo :: TipoDePokemon -> Entrenador -> Int
cantidadDePokemonsDeTipo t (Entrenador n []) = 0
cantidadDePokemonsDeTipo t (Entrenador n (p:ps)) = 
        if t == tipoP p
        then 1+ cantidadDePokemonsDeTipo t (Entrenador n ps)
        else cantidadDePokemonsDeTipo t (Entrenador n ps)

lePuedeGanar :: Entrenador -> Pokemon -> Bool
lePuedeGanar (Entrenador n []) p = False 
lePuedeGanar (Entrenador n (p:ps)) p2 = 
        if leGanaP p p2
        then True
        else lePuedeGanar(Entrenador n ps) p

esExperto :: Entrenador -> Bool
esExperto (Entrenador n ps) = 
        if      (fijarTipoEnLista ps Agua) && 
                (fijarTipoEnLista ps Fuego) &&
                (fijarTipoEnLista ps Planta)
        then True
        else False 

fiestaPokemon :: [Entrenador] -> [Pokemon]
fiestaPokemon [] = []
fiestaPokemon ((Entrenador n ps):es) = append ps (fiestaPokemon es) 

-- 4 --------------------------------------

--ingredientes (Agregar Queso (Agregar Jamon (Prepizza)))
ingredientes :: Pizza -> [Ingrediente]
ingredientes (Prepizza) = []
ingredientes (Agregar a p) = a:(ingredientes p)

tieneJamon :: Pizza -> Bool
tieneJamon (Prepizza) = False
tieneJamon (Agregar a p) = 
        if a == Jamon
        then True
        else tieneJamon p

sacarJamon :: Pizza -> Pizza
sacarJamon (Prepizza) = Prepizza
sacarJamon (Agregar a p) = 
        if a == Jamon
        then sacarJamon(p)
        else Agregar a (sacarJamon (p))

armarPizza :: [Ingrediente] -> Pizza
armarPizza [] = Prepizza
armarPizza (x:xs) = Agregar x (armarPizza xs)

duplicarAceitunas :: Pizza -> Pizza
duplicarAceitunas (Prepizza) = Prepizza
duplicarAceitunas (Agregar (AceitunasVerdes a) p) = Agregar (AceitunasVerdes (a*2)) p

sacar :: [Ingrediente] -> Pizza -> Pizza
sacar [] (Prepizza) = Prepizza
sacar [] (Agregar i p) = Agregar i p
sacar (x:xs) (Agregar i p) = sacar xs (sacarI x (Agregar i p))        

cantJamon :: [Pizza] -> [(Int, Pizza)]
cantJamon [] = []
cantJamon (x:xs) = (cantidadI Jamon x, x):cantJamon xs

mayorNAceitunas :: Int -> [Pizza] -> [Pizza]
mayorNAceitunas n [] = []
mayorNAceitunas n ((Agregar i p):xs) = 
        if cantidadDeAceitunas(AceitunasVerdes n) < cantidadDeAceitunas(i)
        then (Agregar i p):(mayorNAceitunas n xs)
        else mayorNAceitunas n xs
-- mayorNAceitunas 1 [(Agregar (AceitunasVerdes 10) (Agregar Jamon (Prepizza)))]

-- 5 --------------------------------------

hayTesoro :: Camino -> Bool
hayTesoro (Fin) = False
hayTesoro (Cofre [] c) = False
hayTesoro (Cofre (o:os) c) =
        if  o == Tesoro 
        then True
        else hayTesoro (Cofre os c)
hayTesoro (Nada c) = hayTesoro (c)

--pasosHastaTesoro (Nada(Cofre [Tesoro](Fin)))
pasosHastaTesoro :: Camino -> Int
pasosHastaTesoro (Fin) = error "0"
pasosHastaTesoro (Cofre os c) = 0
pasosHastaTesoro (Nada c) = 
        if hayTesoro (c)
        then 1 + pasosHastaTesoro (c)
        else 0

hayTesoroEn :: Int -> Camino -> Bool
hayTesoroEn n (c) = if (pasosHastaTesoro (c)) == n then True else False

alMenosNTesoros :: Int -> Camino -> Bool
alMenosNTesoros n c = 
        if n < cantidadDeTesoros (c)
        then True
        else False

cantTesorosEntre :: Int -> Int -> Camino -> Int
cantTesorosEntre 0 0 (Fin) = 0
cantTesorosEntre 0 0 (Nada c) = 0 
cantTesorosEntre 0 0 (Cofre os c) = 0

cantTesorosEntre 0 y (Nada c) = cantTesorosEntre 0 (y - 1) (c) 
cantTesorosEntre 0 y (Cofre os c) = cantidadDeObjetosTesoro os + cantTesorosEntre 0 (y - 1) (c)

cantTesorosEntre x y (Nada c) = cantTesorosEntre (x-1) y (c) 
cantTesorosEntre x y (Cofre os c) = cantTesorosEntre (x-1) y (c)

-- 6 --------------------------------------


length' :: ListaNoVacia a -> Int
length' (Unit a) = 1
length' (Cons a ls) = 1 + length' ls
-- length' (Cons 0(Cons 1(Cons 2(Cons 3(Unit 4)))))

head' :: ListaNoVacia a -> a
head' (Unit a) = a
head' (Cons a ls) = a

tail' :: ListaNoVacia a -> ListaNoVacia a
tail' (Unit a) = error "ListaNoVacia no admite lista Vacia"
tail' (Cons a ls) = ls

minimo :: ListaNoVacia Int -> Int
minimo (Cons a (Unit a1)) = if a < a1 then a else a1
minimo (Cons a ls) = if a < head' ls then minimo(Cons a (tail' ls)) else minimo ls



-- 7 --------------------------------------
-- data T a =   A 
--             |B a
--             |C a a
--             |D (T a)
--             |E a (T a) 

-- size (D (D (E 1(A))))
size :: T a -> Int
size (A) = 1 
size (B a) = 1
size (C a b) = 1
size (D (t)) = 1 + size t
size (E a (t)) = 1 + size t

-- sum' (D (D (E 1(A))))
sum' :: T Int -> Int
sum' (A) = 0 
sum' (B a) = a
sum' (C a b) = a + b
sum' (D (t)) = sum' t
sum' (E a (t)) = a + sum' t

-- hayD (D (D (E 1(A))))
hayD :: T a -> Bool
hayD (A) = False 
hayD (B a) = False
hayD (C a b) = False
hayD (D (t)) = True
hayD (E a (t)) = hayD (t)

-- cantE (D (D (E 1(A))))
cantE :: T a -> Int
cantE (A) = 0 
cantE (B a) = 0
cantE (C a b) = 0
cantE (D (t)) = 1 + cantE (t)
cantE (E a (t)) = 0 + cantE (t)

-- recolectarC (D (D (E 1(A))))
-- recolectarC :: T a -> (a, a)
recolectarC (C a b) = (a, b)
recolectarC (D (t)) = recolectarC (t)
recolectarC (E a (t)) = recolectarC (t)

-- toList (D (D (E 1(A))))
-- toList :: T a -> [a]
toList (A) = [] 
toList (B a) = [a]
toList (C a b) = [a, b]
toList (D (t)) = toList (t)
toList (E a (t)) = a : toList (t)

-------- Lib -------

len :: [a] -> Int 
len [] = 0
len (x:xs) = 1 + len xs

append :: Eq a => [a]-> [a]-> [a]
append [] ys = ys
append (x:xs) ys = x:append xs ys


tipoP :: Pokemon -> TipoDePokemon
tipoP (Pokemon t p) = t 

fijarTipoEnLista :: [Pokemon] -> TipoDePokemon -> Bool
fijarTipoEnLista [] t = False
fijarTipoEnLista (p:ps) t = 
        if tipoP p == t
        then True
        else fijarTipoEnLista ps t

sacarI :: Ingrediente -> Pizza -> Pizza
sacarI i (Prepizza) = Prepizza
sacarI i (Agregar a p) = 
        if i == a
        then sacarI i (p)
        else Agregar a (sacarI i (p))

cantidadI :: Ingrediente -> Pizza -> Int
cantidadI i (Prepizza) = 0 
cantidadI i (Agregar a p) = 
        if i == a 
        then 1 + cantidadI i (p) 
        else cantidadI i (p)

cantidadDeAceitunas :: Ingrediente -> Int
cantidadDeAceitunas (AceitunasVerdes i) = i

cantidadDeObjetosTesoro :: [Objeto] -> Int
cantidadDeObjetosTesoro [] = 0
cantidadDeObjetosTesoro (o:os) =
        if o == Tesoro
        then 1 + cantidadDeObjetosTesoro os
        else cantidadDeObjetosTesoro os

cantidadDeTesoros :: Camino -> Int
cantidadDeTesoros (Fin) = 0
cantidadDeTesoros (Cofre os c) = (cantidadDeObjetosTesoro os) + cantidadDeTesoros (c)
cantidadDeTesoros (Nada c) = cantidadDeTesoros (c)

