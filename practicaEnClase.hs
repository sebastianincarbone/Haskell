module celda where
    data Color = Azul | Rojo| Verde| Negro
    data Celda = C [(Int,Color)]

    --inv Rep
    --int > 0
    -- [] tiene tantos elementos como colores haya
    --los colores no se repiten en la lista

    celdaVacia :: Celda
    celdaVacia = C [(0,Azul),(0,Rojo),(0,Verde),(0,Negro)]

    poner :: Color -> Celda -> Celda
    poner c  (C xs) = C(poner' c xs)

    poner' :: Color -> [a] -> [a]
    poner' c [] =  []
    poner' c (x:xs) = (ponerEnPar' c x): poner' c xs

    ponerEnPar' :: Color -> (cant,col) -> (cant,col)
    ponerEnPar' c (cant,col) = (if col == c then (cant + 1) else cant, col)

    sacar :: Color -> Celda -> Celda
    sacar c (C xs)= C (sacar' c xs)
    
    sacar' :: Color -> [a] -> [a]
    sacar' c [] =  []
    sacar' c (x:xs) = (sacarEnPar' c x): sacar' c xs

    sacarEnPar' :: Color -> (cant,col) -> (cant,col)
    sacarEnPar' c (cant,col) = (if col == c then if cant > 0 then (cant - 1) else Error 'Boom' else cant, col)

    nrBolitas :: Color -> Celda -> Int
    nrBolitas c (C xs) = nrBolitas' c xs

    nrBolitas' :: Color -> [a] -> Int
    nrBolitas' c [] = []
    nrBolitas' c (x:xs) = (nrBolitasDelPar c x) + nrBolitas' c xs

    nrBolitasDelPar :: Color -> (cant,col) -> Int
    nrBolitasDelPar c (cant,col) = if col == c then cant else 0 