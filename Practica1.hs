--Ejercicio 1
	sucesor :: Int -> Int
	sucesor x = x+1
	
	sumar :: Int -> Int -> Int
	sumar x y = x+y
	
	maximo :: Int -> Int -> Int
	maximo x y = 	if x < y
			then y
			else x
--Ejercicio 2

	negar :: Bool -> Bool
	negar x = not x
	
	andLogico :: Bool -> Bool -> Bool
	andLogico x y = x && y
	
	orLogico :: Bool -> Bool -> Bool
	orLogico x y = x || y

	primera :: (Int,Int) -> Int
	primera xs = fst xs

	segunda :: (Int,Int) -> Int
	segunda xs = snd xs

	sumaPar :: (Int,Int) -> Int
	sumaPar xs = fst xs + snd xs

	maxDelPar :: (Int,Int) -> Int
	maxDelPar xs =  if fst xs <  snd xs
			then snd xs
			else fst xs
			
--Ejercicio 3
	
	loMismo :: a -> a
	loMismo x = x
	
	siempreSiete :: a -> Int
	siempreSiete x = 7

	duplicar :: a -> (a,a)
	duplicar x = (x,x)
	
	singleton :: a -> [a]
	singleton x = [x]
	
--Ejercicio 4

	isEmpty :: [a] -> Bool
	isEmpty [] = False
	isEmpty xs = True

	head' :: [a] -> a
	head'[] = error "Lista vacia"
	head'(x:xs) = x

	tail' :: [a] -> [a]
	tail' [] = error "Lista vacia"
	tail' (x:xs) = xs

--Recurcion
	
	sumatoria :: [Int] -> Int
	sumatoria [] = 0
	sumatoria (n:ns) = n + sumatoria ns

	longitud :: [a] -> Int
	longitud [] = 0
	longitud (n:ns) = 1 + longitud ns

	mapSucesor :: [Int] -> [Int]
	mapSucesor [] = []
	mapSucesor (n:ns) = sucesor n :(mapSucesor ns)

	mapSumaPar :: [(Int,Int)] -> [Int]
	mapSumaPar [] = []
	mapSumaPar (n:ns) = sumaPar n :(mapSumaPar ns)
	
	mapMaxDelPar :: [(Int,Int)] -> [Int]
	mapMaxDelPar [] = []
	mapMaxDelPar (n:ns) = maxDelPar n :(mapMaxDelPar ns)
	
	todoVerdad :: [Bool] -> Bool
	todoVerdad [] = True
	todoVerdad (b:bs) = b && todoVerdad bs 

	algunaVerdad :: [Bool] -> Bool
	algunaVerdad [] = False
	algunaVerdad (b:bs) = b || algunaVerdad bs

	pertenece :: Eq a => a -> [a] -> Bool
	pertenece e [] = False
	pertenece e (x:xs) = 	if e == x
				then True
				else pertenece e xs

	apariciones :: Eq a => a -> [a] -> Int
	apariciones e [] = 0
	apariciones e (x:xs) = 	if e == x
				then 1 + apariciones e xs
				else  apariciones e xs

	filtrarMenoresA :: Int -> [Int] -> [Int]
	filtrarMenoresA e [] = []
	filtrarMenoresA e (n:ns) = 	if e > n
					then n :(filtrarMenoresA e ns)
					else filtrarMenoresA e ns
	
	filtrarElemento :: Eq a => a -> [a] -> [a]
	filtrarElemento e [] = []
	filtrarElemento e (x:xs) = 	if e == x
								then filtrarElemento e xs
								else x :(filtrarElemento e xs)

	mapLongitudes :: [[a]] -> [Int]
	mapLongitudes [] = []
	mapLongitudes (xs:xss) = longitud xs :(mapLongitudes xss)

	longitudMayorA :: Int -> [[a]] -> [[a]]
	longitudMayorA n [] = []
	longitudMayorA n (xs:xss) = if longitud xs <= n
								then longitudMayorA n xss
								else xs :(longitudMayorA n xss)

	intercalar :: a -> [a] -> [a]
	intercalar e [] = []
	intercalar e (x:xs) = x :( e :(intercalar e xs))

	snoc :: [a] -> a -> [a]
	snoc [] e= e :[]
	snoc (x:xs) e = x:(snoc xs e)

	append :: [a] -> [a] -> [a]
	append [] es = es
	append (x:xs) es = x:(append xs es)

	aplanar :: [[a]] -> [a]
	aplanar [] = []
	aplanar (xs:xss) = append xs (aplanar xss)

	traerUltimo :: [a] -> a
	traerUltimo (x:[]) = x
	traerUltimo (x:xs) = traerUltimo xs

	eliminarUltimo :: [a] -> [a]
	eliminarUltimo [] = []
	eliminarUltimo (x:[]) = []
	eliminarUltimo (x:xs) = x:(eliminarUltimo xs)

	reversa :: [a] -> [a]
	reversa [] = []
	reversa xs = traerUltimo xs :(reversa (eliminarUltimo xs))

	zipMaximos :: [Int] -> [Int] -> [Int]
	zipMaximos [] [] = []
	zipMaximos xs [] = xs
	zipMaximos [] ys = ys
	zipMaximos (x:xs) (y:ys) = maximo x y :(zipMaximos xs ys)

	zipSort :: [Int] -> [Int] -> [(Int, Int)]
	zipSort [] [] = []
	zipSort (x:xs) (y:ys) = (x, y) :(zipSort xs ys)
	 
	promedio :: [Int] -> Int
	promedio ns = div (sumatoria ns) (longitud ns)

	minimum' :: Ord a => [a] -> a
	minimum' [] = error"lista vacia"
	minimum' (x:[]) = x
	minimum' (x:xs) = if x < minimum' xs 
						then x
						else minimum' xs

--2.2 Recursión sobre números

	factorial :: Int -> Int
	factorial 0 = 1
	factorial n = n * (factorial (n-1))		

	cuentaRegresiva :: Int -> [Int]
	cuentaRegresiva 0 = []
	cuentaRegresiva n = n :(cuentaRegresiva (n-1))

	contarHasta :: Int -> [Int]
	contarHasta n = reversa(cuentaRegresiva n)

	replicarN :: Int -> a -> [a]
	replicarN 0 n = []
	replicarN e n = n:(replicarN (e-1) n)

	desdeHasta :: Int -> Int -> [Int]
	desdeHasta 0 0 = []
	desdeHasta n m = 	if m == n
						then n :(desdeHasta 0 0)
						else n :(desdeHasta (n+1) m)

	takeN :: Int -> [a] -> [a]
	takeN n [] = []
	takeN 0 xs = []
	takeN n (x:xs) = x:(takeN (n-1) xs)

	dropN :: Int -> [a] -> [a] 
	dropN n [] = []	
	dropN n (x:xs) = 	if n /= 0 
						then dropN (n-1) xs
						else x:(dropN n xs)

	splitN :: Int -> [a] -> ([a], [a])
	splitN n xs = ( takeN n xs , dropN n xs)

--Anexo con ejercicios adicionales

	maximum' :: Ord a => [a] -> a
	maximum' [] = error"lista vacia"
	maximum' (x:[]) = x
	maximum' (x:xs) = if x > maximum' xs 
						then x
						else maximum' xs 
	
	maxM :: [Int] -> Int
	maxM [] = 0
	maxM [x] = x
	maxM (x:y:xs) = maxM ((maximo x y):xs)


	dropElemento :: Eq a => a -> [a] -> [a]
	dropElemento e [] = []
	dropElemento e (x:xs) = 
		if e == x
		then (dropElemento e xs)
		else x:(dropElemento e xs)
	
	splitMin :: Ord a => [a] -> (a, [a])
	splitMin xs = (minimum' xs, dropElemento (minimum' xs) xs)

	ordenar :: Ord a => [a] -> [a]
	ordenar [] = [] 
	ordenar (x:xs) = (minimum (x:xs)):(ordenar (dropElemento (minimum' (x:xs)) (x:xs)))

	interseccion :: Eq a => [a] -> [a] -> [a]
	interseccion [] [] = []
	interseccion xs [] = []
	interseccion [] ys = []
	interseccion (x:xs) (y:ys) = if x == y
							 	 then x:(interseccion xs ys)
								 else (interseccion xs ys)
								  
	existElement :: Eq a => a -> [a] -> Bool 
	existElement x [] = False
	existElement x (y:ys) = if x == y
							then True || existElement x ys
							else existElement x ys

	diferencia :: Eq a => [a] -> [a] -> [a]
	diferencia [] [] = []
	diferencia [] ys = ys
	diferencia (x:xs) ys =  if existElement x ys
								then diferencia xs (dropElemento x ys)
								else diferencia xs ys

	filtrarMayoresA :: Int -> [Int] -> [Int]
	filtrarMayoresA e [] = []
	filtrarMayoresA e (n:ns) = 	if e < n
					then n :(filtrarMayoresA e ns)
					else filtrarMayoresA e ns
	
	particionPorSigno :: [Int] -> ([Int], [Int])
	particionPorSigno ns = (filtrarMenoresA 0 ns, filtrarMayoresA 0 ns )

	numerosPares :: [Int] -> [Int]
	numerosPares [] = []
	numerosPares (n:ns) = if (mod n 2) == 0
						then n:numerosPares ns
						else numerosPares ns
	
	numerosImpares :: [Int] -> [Int]
	numerosImpares [] = []
	numerosImpares (n:ns) = if (mod n 2) /= 0
						then n:numerosImpares ns
						else numerosImpares ns

	particionPorParidad :: [Int] -> ([Int], [Int])
	particionPorParidad ns = (numerosImpares ns, numerosPares ns)

	subtails :: [a] -> [[a]]
	subtails [] = []
	subtails xs = xs:subtails (tail xs) 

	agrupar :: Eq a => [a] -> [[a]]
	agrupar (x:[]) = []
	agrupar (x:a:xs) = if x == a
					 then (x:a:[]):agrupar xs
					 else (x:[]):agrupar xs

	eqList :: Eq a => [a] -> [a] -> Bool
	eqList [] [] = True
	eqList [] ys = False
	eqList xs [] = False
	eqList (x:xs) (y:ys) = if x == y
							then True && eqList xs ys 
							else False &&  eqList xs ys
	
	esPrefijo :: Eq a => [a] -> [a] -> Bool
	esPrefijo [] ys = False
	esPrefijo xs [] = False
	esPrefijo xs ys = eqList xs (dropN ((longitud ys) - (longitud xs)) ys) 
	

	esSufijo :: Eq a => [a] -> [a] -> Bool
	esSufijo [] ys = False
	esSufijo xs [] = False
	esSufijo xs ys = eqList xs (takeN (longitud xs) ys) 
	