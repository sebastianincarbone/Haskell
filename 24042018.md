# PREMISAS
## 1 COMPLEJIDAD TEMPORAL
###     cantidad de operaciones elementales en funcion del tamaño de los datos
        --(estudiar el comportamiento del programa cuando los datos crecen)
        --complejidad constante (y = x) -> O(1)
        --complejidad lineal (y = ax + b) -> O(n)
        --complejidad cuadratica (y = a x^2 + bx + c) -> O(n^2)

## 2 COMPORTAMIENTO ASINTOTICO
###     (como se comportan los programas cuando los datos son muy grandes)
    --queremos distinguir un programa que tarda segundos de otro que tarda años.
    --no nos va a importar si el programa tarda 10 veces mas o menos.
    --nos va a importar estudiar como se comporta el ptrograma a medida que los datos sean mas grandes.

## 3 PEOR CASO
###     Ej. sonTodosPares :: [Int] -> Bool
        --puedo cortar en el primer elemento impar 
        --o
        --puedo ver todos > siendo el peor caso


