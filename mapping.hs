--DICCIONARIO
module Mapping() where  

import set

data Map k v = M [(k, v)]

------------------------ CASO DONDE NO ME IMPOERTA EL DOM

emptyM :: Map k v
--O(1)
emptyM = M []

assocM:: Map k v -> k -> v -> Map k v 
--O(1)
assocM (M kvs) k v = M ((k,v):kvs) 

deleteM::Map k v -> k -> Map k v
--O(m) siendo m la cantidad de elementos de la lista
deleteM (M kvs) k = deleteMAux kvs k

deleteMAux :: Eq k => [(k,v)] -> k -> [(k, v)]
deleteMAux [] k = []
deleteMAux ((k1,v1):kvs) k = 
    if k1 == k 
    then deleteMAux kvs k
    else (k1,v1):(deleteMAux kvs k)

lookupM:: Map k v -> k -> Maybe v
--O(m) siendo m la cantidad de elementos de la lista
lookupM (M kvs) k = lookupMAux(kvs k)  

lookupMAux :: Eq k -> [(k, v)] -> k -> Maybe v
lookupMAux [] k = Nothing
lookupMAux ((k1,v1):kvs) k = 
    if k1 == k 
    then Just v1
    else lookupMAux kvs k

domM:: Mapk k v -> [k]
--O(m * n) siendo m la cantidad de elementos de la lista y n la cantidad de claves
domM (M kvs) =  domMAux kvs

domMAux :: [(k, v)] -> [k]
domMAux kvs = setToList( armarSetClaves kvs)

armarSetClaves :: [(k, v)] -> Set k
armarSetClaves [] = emptyS
armarSetClaves ((k,v):kvs) = addS k (armarSetClaves kvs) 

------------------------ CASO CON INVARIANTE DE REPRESENTACION
--INV REP no hay claves repetidas
emptyM' :: Map k v
--O(1)
emptyM' = M []

assocM':: Map k v -> k -> v -> Map k v 
--O(1)
assocM' (M kvs) k v = M (assocMAux' kvs k v) 

assocMAux' :: Eq k => [(k,v)] -> k -> v -> [(k,v)]
assocMAux' (M []) k v = [(k, v)] 
assocMAux' (M (k1, v1):kvs) k v = 
    if k == k1
    then (k, v): kvs  
    else (k1, v1): assocMAux' kvs k v 

deleteM'::Map k v -> k -> Map k v
--O(m) siendo m la cantidad de elementos de la lista
deleteM' (M kvs) k = deleteMAux' kvs k

deleteMAux' :: Eq k => [(k,v)] -> k -> [(k, v)]
deleteMAux' [] k = []
deleteMAux' ((k1,v1):kvs) k = 
    if k1 == k 
    then deleteMAux' kvs
    else (k1,v1):(deleteMAux' kvs k)

lookupM':: Map k v -> k -> Maybe v
--O(m) siendo m la cantidad de elementos de la lista
lookupM' (M kvs) k = lookupMAux'(kvs k)  

lookupMAux' :: Eq k -> [(k, v)] -> k -> Maybe v
lookupMAux' [] k = Nothing
lookupMAux' ((k1,v1):kvs) k = 
    if k1 == k 
    then Just v1
    else lookupMAux' kvs k

domM':: Mapk k v -> [k]
--O(m * n) siendo m la cantidad de elementos de la lista y n la cantidad de claves
domM' (M kvs) =  domMAux' kvs

domMAux' :: [(k, v)] -> [k]
domMAux' (M []) = []  
domMAux' (M (k, v):kvs) = k: domMAux' kvs 