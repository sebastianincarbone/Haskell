headM :: [a] -> Maybe a
headM [] = Nothing
headM (x:xs) = Just x

lastM :: [a] -> Maybe a
lastM [] = Nothing
lastM xs = Just (last xs)

maximumM :: Ord a => [a] -> Maybe a
maximumM [] = Nothing
maximumM xs = Just (maximum xs)

initM :: [a] -> Maybe [a]
initM [] = Nothing
initM xs = Just (init xs) 

get :: Int -> [a] -> Maybe a
get a [] = Nothing
get a xs = get' a xs

get' :: Int -> [a] -> Maybe a
get' a [] = Nothing
get' 0 (x:xs) = Just x 
get' a (x:xs) = get' (a-1) xs  

indiceDe :: Eq a => a -> [a] -> Maybe Int
indiceDe a [] = Nothing 
indiceDe a xs = indiceDe' a 0 xs

indiceDe' :: Eq a => a -> Int -> [a] -> Maybe Int
indiceDe' a b [] = Nothing
indiceDe' a b (x:xs) =  if a == x 
                        then Just b
                        else indiceDe' a (b+1) xs

lookupM :: Eq k => [(k,v)] -> k -> Maybe v
lookupM [] a = Nothing
lookupM xs a = lookupM' xs a

lookupM' :: Eq k => [(k,v)] -> k -> Maybe v
lookupM' [] a = Nothing
lookupM' ((k,v):xs) a = if k == a then Just v else lookupM' xs a

fromJusts :: [Maybe a] -> [a]
fromJusts [] = []
fromJusts ((Just a):xs) = a:fromJusts xs
fromJusts (Nothing:xs) = fromJusts xs 

data Tree a = EmptyT |NodeT a (Tree a) (Tree a)

varTree :: Tree Int
varTree = (NodeT 11 (NodeT 21 (NodeT 3 (EmptyT) (EmptyT)) (EmptyT)) (NodeT 4 EmptyT EmptyT))

minT :: Ord a => Tree a -> Maybe a
minT EmptyT = Nothing
minT t = Just (mint' t)

mint' :: Ord a => Tree a -> a
mint' (NodeT a EmptyT EmptyT) = a
mint' (NodeT a EmptyT br) = min a (mint' br)
mint' (NodeT a bl EmptyT) = min a (mint' bl)
mint' (NodeT a bl br) = min a (min (mint' bl)(mint' br))

maxT :: Ord a => Tree a -> Maybe a
maxT EmptyT = Nothing
maxT t = Just (maxT' t)

maxT' :: Ord a => Tree a -> a
maxT' (NodeT a EmptyT EmptyT) = a
maxT' (NodeT a bl EmptyT) = max a (maxT' bl)
maxT' (NodeT a EmptyT br) = max a (maxT' br)
maxT' (NodeT a bl br) = max a (max (maxT' bl) (maxT' br))

------Ejer 2 Costos

--head’ :: [a] -> a
--head’ (x:xs) = x

--sumar :: Int -> Int
--sumar x = x + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1

--factorial :: Int -> Int
--factorial 0 = 1
--factorial n = n * factorial (n-1)

--longitud :: [a] -> Int
--longitud [] = 0
--longitud (x:xs) = 1 + longitud xs

--factoriales :: [Int] -> [Int]
--factoriales [] = []
--factoriales (x:xs) = factorial x : factoriales xs

--pertenece :: Eq a => a -> [a] -> Bool
--pertenece n [] = False
--pertenece n (x:xs) = n == x || pertenece n xs

--sinRepetidos :: Eq a => [a] -> [a]
--sinRepetidos [] = []
--sinRepetidos (x:xs) =
--  if pertenece x xs
--  then sinRepetidos xs
--  else x : sinRepetidos xs

---- equivalente a (++)
--append :: [a] -> [a] -> [a]
--append [] ys = ys
--append (x:xs) ys = x : append xs ys

--concatenar :: [String] -> String
--concatenar [] = []
--concatenar (x:xs) = x ++ concatenar xs

--takeN :: Int -> [a] -> [a]
--takeN 0 xs = []
--takeN n [] = []
--takeN n (x:xs) = x : takeN (n-1) xs

--dropN :: Int -> [a] -> [a]
--dropN 0 xs = xs
--dropN n [] = []
--dropN n (x:xs) = dropN (n-1) xs

--partir :: Int -> [a] -> ([a], [a])
--partir n xs = (takeN n xs, dropN n xs)

--minimo :: Ord a => [a] -> a
--minimo [x] = x
--minimo (x:xs) = min x (minimo xs)

--sacar :: Eq a => a -> [a] -> [a]
--sacar n [] = []
--sacar n (x:xs) =
--  if n == x
--  then xs
--  else x : sacar n xs

--orderar :: Ord a => [a] -> [a]
--orderar [] = []
--orderar xs =
--  let m = minimo xs
--      in m : ordenar (sacar m xs)


------Ejer 3 Conjunto
--import Set

--losQuePertenecen :: Eq a => [a] -> Set a -> [a]


--sinRepetidos :: Eq a => [a] -> [a]
--unirTodos :: Eq a => Tree (Set a) -> Set a

------Ejer 4 Conjunto
--import Queue

--largoQ :: Queue a -> Int
--atender :: Queue String -> [String]
--unirQ :: Queue a -> Queue a -> Queue a

------Ejer 5 Stack
--import stack

--apilar :: [a] -> Stack a
--desapilar :: Stack a -> [a]
--treeToStack :: Tree a -> Stack a
--balanceado :: String -> Bool (desafío)

------Ejer 6 Queue con longitud constante

--lenQ :: Queue a -> Int -- O(1)

------Ejer 7 Conjunto con Máximo

--maximoC :: Ord a => Set a -> a

------Ejer 8 Stack con máximo en tiempo constante

--maxS :: Ord a => Stack a -> a
--push :: Ord a => a -> Stack a -> Stack a

------------clase 24-4-18
--Operaciones aritmeticas
-- para cada operacion cuesta 1 Operacion elemental
--Ej1. 4+1 > cuesta 1 Operacion elemental
--Ej2. if 4-1 == 2 then 3 else 5+5 > cuesta 4 Operacion elemental
--cada que construis una tupla cuesta 1 Operacion elemental
--cada que aplicas un cons [':'] cuesta 1
--Llamar a una funcion 
    --f[] = 21 + 21
    --f(x:xs) = (x+x) * f xs 
        --cuesta 14
