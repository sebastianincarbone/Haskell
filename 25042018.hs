import mapping

type Palabra = String
type Significado = String

type Diccionario = Map Palabra [Significado]

--buscar :: Diccionario -> palabra -> [significado]
--buscar dicc p = darLaLista(lookupM dicc p) 
--AUX
--darLaLista :: Maybe [v] -> [v]
--darLaLista Nothing = []
--darLaLista (Just vs) = vs

buscar :: Diccionario -> Palabra -> [Significado]
buscar dicc p = 
    case (lookupM dicc p) of 
        Nothing -> []
        Just vs -> vs

agregar :: Diccionario -> Palabra -> Significado -> Diccionario
agregar dicc p s = 
        case lookupM dicc p of
        Nothing = assocM dicc p [s]
        Just vs = assocM dicc p (s:vs)

cuantasPalabras :: Diccionario -> Int
cuantasPalabras dcc = length(domM dicc) 